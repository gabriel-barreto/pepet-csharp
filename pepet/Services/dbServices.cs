﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Windows.Forms;

namespace pepet.Services
{
    class dbServices
    {
        private string connectionString { get; set; }
        private MongoServer server { get; set; }
        public MongoDatabase db { get; set; }


        public void connection() {
            try
            {
                connectionString = "mongodb://gabriel-barreto:Gabriel-001@cluster0-shard-00-00-m0ay0.mongodb.net:27017,cluster0-shard-00-01-m0ay0.mongodb.net:27017,cluster0-shard-00-02-m0ay0.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";
                MongoClient mongoClient = new MongoClient(connectionString);
                server = mongoClient.GetServer();
                db = server.GetDatabase("pepet");
            }
            catch (Exception ex) {
                MessageBox.Show("Nao foi possivel estabelecer conexao com o servidor do banco de dados\nException: "+ex, "Error");
            }
        }
    }
}
