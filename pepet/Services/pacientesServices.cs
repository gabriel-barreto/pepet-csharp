﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Services
{
    class pacientesServices
    {
        Services.dbServices dbServices = new dbServices();

        public Models.Pacientes getPacienteByCode(string pacienteCode) {
            dbServices.connection();
            var paciente = new Models.Pacientes();
            var collection = dbServices.db.GetCollection<Models.Pacientes>("pacientes");
            if (collection == null) { MessageBox.Show("Nenhum colecao foi encontrado!", "404 - Not Found"); }
            else
            {
                var query = Query.EQ("code", pacienteCode);
                paciente = collection.FindOne(query);
                if (paciente == null)
                {
                    MessageBox.Show("Nenhum documento foi encontrado!", "404 - Not Found");
                }
            }
            return paciente;
        }

        public Models.Pacientes getPacienteByName(string pacienteName) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Pacientes>("pacientes");
            var query = Query.EQ("nome", pacienteName);
            var paciente = collection.FindOne(query);
            if (paciente == null) {
                MessageBox.Show("Nenhum documento foi encontrado", "404 - Not Found");
            }
            return paciente;
        }

        public void addPaciente(Models.Pacientes newPaciente) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Pacientes>("pacientes");
            collection.Insert(newPaciente);
            var _id = newPaciente._id;
        }

        public void deletePaciente(string pacienteCode) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Pacientes>("pacientes");
            var query = Query.EQ("code", pacienteCode);
            collection.Remove(query);
        }
    }
}
