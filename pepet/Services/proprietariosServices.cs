﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Services
{
    class proprietariosServices
    {
        Services.dbServices dbServices = new dbServices();

        public void addProprietario(Models.Proprietarios newProprietario) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Proprietarios>("proprietarios");
            collection.Insert(newProprietario);
            MessageBox.Show("Proprietário incluido com sucesso!", "Sucesso!");
        }

        public Models.Proprietarios getProprietarioByCpf(string proprietarioCode) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Proprietarios>("proprietarios");
            var query = Query.EQ("cpf", proprietarioCode);
            var prop = collection.FindOne(query);
            return prop;
        }

        public void deleteProprietario(string proprietarioCode) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Proprietarios>("proprietarios");
            var query = Query.EQ("cpf", proprietarioCode);
            collection.Remove(query);
        }
    }
}
