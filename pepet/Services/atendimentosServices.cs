﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Services
{
    class atendimentosServices
    {

        dbServices dbServices = new dbServices();

        public Models.Analises getLastAtendimento(string pacienteCode) {
            dbServices.connection();
            var lastAtendimento = new Models.Analises();
            var collection = dbServices.db.GetCollection<Models.Analises>("analises");
            var query = Query.EQ("animal", pacienteCode);
            var atendimentos = collection.Find(query).ToArray<Models.Analises>();
            if (atendimentos.Length > 0)
            {
                lastAtendimento = atendimentos[atendimentos.Length - 1];
            }
            return lastAtendimento;
        }

        public void addAtendimento(Models.Analises newAtendimento) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Analises>("analises");
            collection.Insert(newAtendimento);
            MessageBox.Show("Análise incluida com sucesso!", "Sucesso!");
        }

        public Models.Analises getAtendimentoByCode(string atendimentoCode) {
            dbServices.connection();
            var collection = dbServices.db.GetCollection<Models.Analises>("analises");
            var query = Query.EQ("code", atendimentoCode);
            var atendimento = collection.FindOne(query);
            return atendimento;
        }
    }
}
