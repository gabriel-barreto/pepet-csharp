﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace pepet.Services
{
    class cepServices
    {
        public string[] getAddress(String cep) {
            string[] address = new String[4];
            string jsonResponse = string.Empty;
            string url = @"http://viacep.com.br/ws/"+cep+"/json/";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                jsonResponse = reader.ReadToEnd();
            }

            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            dynamic jsonResult = serializer.DeserializeObject(jsonResponse);

            foreach (KeyValuePair<string, object> entry in jsonResult)
            {
                var key = entry.Key;
                var value = entry.Value as string;
                if (key == "logradouro")
                {
                    address[0] = value;
                }
                if (key == "bairro")
                {
                    address[1] = value;
                }
                if (key == "localidade")
                {
                    address[2] = value;
                }
                if (key == "uf")
                {
                    address[3] = value;
                }
            }
            return address;
        }
    }
}
