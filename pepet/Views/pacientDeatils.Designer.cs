﻿namespace pepet.Views
{
    partial class pacientDeatils
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pacientDeatils));
            this.panelControls = new System.Windows.Forms.Panel();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelCode = new System.Windows.Forms.Label();
            this.textCode = new System.Windows.Forms.Label();
            this.labelNome = new System.Windows.Forms.Label();
            this.labelAnimal = new System.Windows.Forms.Label();
            this.textAnimal = new System.Windows.Forms.TextBox();
            this.labelSangue = new System.Windows.Forms.Label();
            this.textSangue = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cpfLink = new System.Windows.Forms.LinkLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.textProperty1Phone = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textProperty1Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textNascimento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textSexo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textCor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textRaca = new System.Windows.Forms.TextBox();
            this.labelRaca = new System.Windows.Forms.Label();
            this.textNome = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelObs = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelPrescricao = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelExames = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelSistemas = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelSintomas = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelData = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.linkLabelCodeAtendimento = new System.Windows.Forms.LinkLabel();
            this.panelControls.SuspendLayout();
            this.titlePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.buttonDelete);
            this.panelControls.Location = new System.Drawing.Point(17, 520);
            this.panelControls.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(800, 79);
            this.panelControls.TabIndex = 0;
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.buttonDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.buttonDelete.ForeColor = System.Drawing.Color.White;
            this.buttonDelete.Location = new System.Drawing.Point(530, 21);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(250, 37);
            this.buttonDelete.TabIndex = 2;
            this.buttonDelete.Text = "Deletar Paciente";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(188)))), ((int)(((byte)(156)))));
            this.titlePanel.Controls.Add(this.labelTitle);
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(836, 53);
            this.titlePanel.TabIndex = 1;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Pangram Medium", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(1)), true);
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(836, 53);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "DETALHES DO PACIENTE";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCode
            // 
            this.labelCode.AutoSize = true;
            this.labelCode.Location = new System.Drawing.Point(29, 21);
            this.labelCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCode.Name = "labelCode";
            this.labelCode.Size = new System.Drawing.Size(60, 18);
            this.labelCode.TabIndex = 3;
            this.labelCode.Text = "Código:";
            // 
            // textCode
            // 
            this.textCode.AutoSize = true;
            this.textCode.Location = new System.Drawing.Point(117, 21);
            this.textCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textCode.Name = "textCode";
            this.textCode.Size = new System.Drawing.Size(16, 18);
            this.textCode.TabIndex = 4;
            this.textCode.Text = "1";
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(29, 64);
            this.labelNome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(57, 18);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Nome: ";
            // 
            // labelAnimal
            // 
            this.labelAnimal.AutoSize = true;
            this.labelAnimal.Location = new System.Drawing.Point(277, 21);
            this.labelAnimal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAnimal.Name = "labelAnimal";
            this.labelAnimal.Size = new System.Drawing.Size(60, 18);
            this.labelAnimal.TabIndex = 7;
            this.labelAnimal.Text = "Animal:";
            // 
            // textAnimal
            // 
            this.textAnimal.Enabled = false;
            this.textAnimal.Location = new System.Drawing.Point(363, 18);
            this.textAnimal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textAnimal.Name = "textAnimal";
            this.textAnimal.Size = new System.Drawing.Size(141, 25);
            this.textAnimal.TabIndex = 8;
            // 
            // labelSangue
            // 
            this.labelSangue.AutoSize = true;
            this.labelSangue.Location = new System.Drawing.Point(547, 21);
            this.labelSangue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSangue.Name = "labelSangue";
            this.labelSangue.Size = new System.Drawing.Size(63, 18);
            this.labelSangue.TabIndex = 9;
            this.labelSangue.Text = "Sangue:";
            // 
            // textSangue
            // 
            this.textSangue.Enabled = false;
            this.textSangue.Location = new System.Drawing.Point(637, 18);
            this.textSangue.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textSangue.Name = "textSangue";
            this.textSangue.Size = new System.Drawing.Size(141, 25);
            this.textSangue.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cpfLink);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textProperty1Phone);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textProperty1Name);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textNascimento);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textSexo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textCor);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textRaca);
            this.panel1.Controls.Add(this.labelRaca);
            this.panel1.Controls.Add(this.textSangue);
            this.panel1.Controls.Add(this.labelSangue);
            this.panel1.Controls.Add(this.textAnimal);
            this.panel1.Controls.Add(this.labelAnimal);
            this.panel1.Controls.Add(this.textNome);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Controls.Add(this.textCode);
            this.panel1.Controls.Add(this.labelCode);
            this.panel1.Location = new System.Drawing.Point(17, 59);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 230);
            this.panel1.TabIndex = 0;
            // 
            // cpfLink
            // 
            this.cpfLink.AutoSize = true;
            this.cpfLink.Location = new System.Drawing.Point(675, 181);
            this.cpfLink.Name = "cpfLink";
            this.cpfLink.Size = new System.Drawing.Size(103, 18);
            this.cpfLink.TabIndex = 25;
            this.cpfLink.TabStop = true;
            this.cpfLink.Text = "111.111.111-11";
            this.cpfLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.goToProprietario);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(626, 181);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 18);
            this.label7.TabIndex = 24;
            this.label7.Text = "CPF: ";
            // 
            // textProperty1Phone
            // 
            this.textProperty1Phone.Enabled = false;
            this.textProperty1Phone.Location = new System.Drawing.Point(434, 178);
            this.textProperty1Phone.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textProperty1Phone.Name = "textProperty1Phone";
            this.textProperty1Phone.Size = new System.Drawing.Size(155, 25);
            this.textProperty1Phone.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(356, 181);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 18);
            this.label6.TabIndex = 22;
            this.label6.Text = "Telefone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 150);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 18);
            this.label5.TabIndex = 21;
            this.label5.Text = "Proprietário:";
            // 
            // textProperty1Name
            // 
            this.textProperty1Name.Enabled = false;
            this.textProperty1Name.Location = new System.Drawing.Point(113, 178);
            this.textProperty1Name.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textProperty1Name.Name = "textProperty1Name";
            this.textProperty1Name.Size = new System.Drawing.Size(205, 25);
            this.textProperty1Name.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 181);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 18);
            this.label4.TabIndex = 19;
            this.label4.Text = "Nome:";
            // 
            // textNascimento
            // 
            this.textNascimento.Enabled = false;
            this.textNascimento.Location = new System.Drawing.Point(637, 100);
            this.textNascimento.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textNascimento.Name = "textNascimento";
            this.textNascimento.Size = new System.Drawing.Size(141, 25);
            this.textNascimento.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(509, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 18);
            this.label3.TabIndex = 17;
            this.label3.Text = "Nascimento:";
            // 
            // textSexo
            // 
            this.textSexo.Enabled = false;
            this.textSexo.Location = new System.Drawing.Point(344, 100);
            this.textSexo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textSexo.Name = "textSexo";
            this.textSexo.Size = new System.Drawing.Size(141, 25);
            this.textSexo.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 103);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Sexo:";
            // 
            // textCor
            // 
            this.textCor.Enabled = false;
            this.textCor.Location = new System.Drawing.Point(113, 100);
            this.textCor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textCor.Name = "textCor";
            this.textCor.Size = new System.Drawing.Size(141, 25);
            this.textCor.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 106);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Cor:";
            // 
            // textRaca
            // 
            this.textRaca.Enabled = false;
            this.textRaca.Location = new System.Drawing.Point(417, 60);
            this.textRaca.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textRaca.Name = "textRaca";
            this.textRaca.Size = new System.Drawing.Size(361, 25);
            this.textRaca.TabIndex = 12;
            // 
            // labelRaca
            // 
            this.labelRaca.AutoSize = true;
            this.labelRaca.Location = new System.Drawing.Point(351, 64);
            this.labelRaca.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRaca.Name = "labelRaca";
            this.labelRaca.Size = new System.Drawing.Size(48, 18);
            this.labelRaca.TabIndex = 11;
            this.labelRaca.Text = "Raça:";
            // 
            // textNome
            // 
            this.textNome.Enabled = false;
            this.textNome.Location = new System.Drawing.Point(113, 60);
            this.textNome.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(205, 25);
            this.textNome.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.linkLabelCodeAtendimento);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.labelObs);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.labelPrescricao);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.labelExames);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.labelSistemas);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.labelSintomas);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.labelData);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(17, 295);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 219);
            this.panel2.TabIndex = 2;
            // 
            // labelObs
            // 
            this.labelObs.AutoSize = true;
            this.labelObs.Location = new System.Drawing.Point(142, 183);
            this.labelObs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelObs.Name = "labelObs";
            this.labelObs.Size = new System.Drawing.Size(126, 18);
            this.labelObs.TabIndex = 16;
            this.labelObs.Text = "Agendar retorno.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(29, 183);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 18);
            this.label20.TabIndex = 15;
            this.label20.Text = "Observações:";
            // 
            // labelPrescricao
            // 
            this.labelPrescricao.AutoSize = true;
            this.labelPrescricao.Location = new System.Drawing.Point(120, 154);
            this.labelPrescricao.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPrescricao.Name = "labelPrescricao";
            this.labelPrescricao.Size = new System.Drawing.Size(577, 18);
            this.labelPrescricao.TabIndex = 14;
            this.labelPrescricao.Text = "Imobilização total da pata fraturada e blablabla a cada 8h durante 1 semana";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(29, 154);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 18);
            this.label18.TabIndex = 13;
            this.label18.Text = "Prescrição:";
            // 
            // labelExames
            // 
            this.labelExames.AutoSize = true;
            this.labelExames.Location = new System.Drawing.Point(103, 126);
            this.labelExames.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExames.Name = "labelExames";
            this.labelExames.Size = new System.Drawing.Size(277, 18);
            this.labelExames.TabIndex = 12;
            this.labelExames.Text = "Raio-x total da pata dianteira direita";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 126);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 18);
            this.label15.TabIndex = 11;
            this.label15.Text = "Exames:";
            // 
            // labelSistemas
            // 
            this.labelSistemas.AutoSize = true;
            this.labelSistemas.Location = new System.Drawing.Point(197, 97);
            this.labelSistemas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSistemas.Name = "labelSistemas";
            this.labelSistemas.Size = new System.Drawing.Size(238, 18);
            this.labelSistemas.TabIndex = 10;
            this.labelSistemas.Text = "Pata dianteira direita fraturada.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 97);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(160, 18);
            this.label13.TabIndex = 9;
            this.label13.Text = "Sistemas Acometidos:";
            // 
            // labelSintomas
            // 
            this.labelSintomas.AutoSize = true;
            this.labelSintomas.Location = new System.Drawing.Point(112, 70);
            this.labelSintomas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSintomas.Name = "labelSintomas";
            this.labelSintomas.Size = new System.Drawing.Size(65, 18);
            this.labelSintomas.TabIndex = 8;
            this.labelSintomas.Text = "Nenhum";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 70);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 18);
            this.label11.TabIndex = 7;
            this.label11.Text = "Sintomas:";
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Location = new System.Drawing.Point(84, 42);
            this.labelData.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(144, 18);
            this.labelData.TabIndex = 6;
            this.labelData.Text = "10/10/2010, 8:47 M";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Pangram", 13.5F);
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(800, 22);
            this.label9.TabIndex = 5;
            this.label9.Text = "DETALHES DO ÚLTIMO ATENDIMENTO:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 42);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 18);
            this.label8.TabIndex = 4;
            this.label8.Text = "Data:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(696, 42);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 18);
            this.label12.TabIndex = 17;
            this.label12.Text = "Code:";
            // 
            // linkLabelCodeAtendimento
            // 
            this.linkLabelCodeAtendimento.AutoSize = true;
            this.linkLabelCodeAtendimento.Location = new System.Drawing.Point(753, 42);
            this.linkLabelCodeAtendimento.Name = "linkLabelCodeAtendimento";
            this.linkLabelCodeAtendimento.Size = new System.Drawing.Size(16, 18);
            this.linkLabelCodeAtendimento.TabIndex = 18;
            this.linkLabelCodeAtendimento.TabStop = true;
            this.linkLabelCodeAtendimento.Text = "1";
            this.linkLabelCodeAtendimento.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelCodeAtendimento_LinkClicked);
            // 
            // pacientDeatils
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(834, 607);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.titlePanel);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pangram", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "pacientDeatils";
            this.panelControls.ResumeLayout(false);
            this.titlePanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label labelCode;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.Label labelAnimal;
        private System.Windows.Forms.Label labelSangue;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelRaca;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label textCode;
        public System.Windows.Forms.TextBox textAnimal;
        public System.Windows.Forms.TextBox textSangue;
        public System.Windows.Forms.TextBox textNome;
        public System.Windows.Forms.TextBox textRaca;
        public System.Windows.Forms.TextBox textNascimento;
        public System.Windows.Forms.TextBox textSexo;
        public System.Windows.Forms.TextBox textCor;
        public System.Windows.Forms.TextBox textProperty1Name;
        public System.Windows.Forms.TextBox textProperty1Phone;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.LinkLabel cpfLink;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label labelObs;
        public System.Windows.Forms.Label labelPrescricao;
        public System.Windows.Forms.Label labelExames;
        public System.Windows.Forms.Label labelSistemas;
        public System.Windows.Forms.Label labelSintomas;
        public System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.LinkLabel linkLabelCodeAtendimento;
    }
}