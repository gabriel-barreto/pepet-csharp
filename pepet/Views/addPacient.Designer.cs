﻿namespace pepet.Views
{
    partial class addPacient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addPacient));
            this.panelControls = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelCode = new System.Windows.Forms.Label();
            this.labelNome = new System.Windows.Forms.Label();
            this.labelAnimal = new System.Windows.Forms.Label();
            this.textAnimal = new System.Windows.Forms.TextBox();
            this.labelSangue = new System.Windows.Forms.Label();
            this.textSangue = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textProp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textNasci = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textSexo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textCor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textRaca = new System.Windows.Forms.TextBox();
            this.labelRaca = new System.Windows.Forms.Label();
            this.textNome = new System.Windows.Forms.TextBox();
            this.panelControls.SuspendLayout();
            this.titlePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.buttonSave);
            this.panelControls.Location = new System.Drawing.Point(12, 273);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(560, 72);
            this.panelControls.TabIndex = 0;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(371, 20);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(175, 35);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Salvar Alterações";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.savePacient);
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(188)))), ((int)(((byte)(156)))));
            this.titlePanel.Controls.Add(this.labelTitle);
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(585, 50);
            this.titlePanel.TabIndex = 1;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(12, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(560, 50);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ADIÇÃO DE PACIENTE";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCode
            // 
            this.labelCode.AutoSize = true;
            this.labelCode.Location = new System.Drawing.Point(20, 20);
            this.labelCode.Name = "labelCode";
            this.labelCode.Size = new System.Drawing.Size(56, 19);
            this.labelCode.TabIndex = 3;
            this.labelCode.Text = "Código:";
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(20, 60);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(53, 19);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Nome: ";
            // 
            // labelAnimal
            // 
            this.labelAnimal.AutoSize = true;
            this.labelAnimal.Location = new System.Drawing.Point(194, 20);
            this.labelAnimal.Name = "labelAnimal";
            this.labelAnimal.Size = new System.Drawing.Size(54, 19);
            this.labelAnimal.TabIndex = 7;
            this.labelAnimal.Text = "Animal:";
            // 
            // textAnimal
            // 
            this.textAnimal.Location = new System.Drawing.Point(254, 17);
            this.textAnimal.Name = "textAnimal";
            this.textAnimal.Size = new System.Drawing.Size(100, 25);
            this.textAnimal.TabIndex = 8;
            // 
            // labelSangue
            // 
            this.labelSangue.AutoSize = true;
            this.labelSangue.Location = new System.Drawing.Point(383, 20);
            this.labelSangue.Name = "labelSangue";
            this.labelSangue.Size = new System.Drawing.Size(57, 19);
            this.labelSangue.TabIndex = 9;
            this.labelSangue.Text = "Sangue:";
            // 
            // textSangue
            // 
            this.textSangue.Location = new System.Drawing.Point(446, 17);
            this.textSangue.Name = "textSangue";
            this.textSangue.Size = new System.Drawing.Size(100, 25);
            this.textSangue.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxCode);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textProp);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textNasci);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textSexo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textCor);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textRaca);
            this.panel1.Controls.Add(this.labelRaca);
            this.panel1.Controls.Add(this.textSangue);
            this.panel1.Controls.Add(this.labelSangue);
            this.panel1.Controls.Add(this.textAnimal);
            this.panel1.Controls.Add(this.labelAnimal);
            this.panel1.Controls.Add(this.textNome);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Controls.Add(this.labelCode);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 211);
            this.panel1.TabIndex = 0;
            // 
            // textBoxCode
            // 
            this.textBoxCode.Location = new System.Drawing.Point(79, 17);
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.Size = new System.Drawing.Size(75, 25);
            this.textBoxCode.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 19);
            this.label5.TabIndex = 21;
            this.label5.Text = "Proprietário(s):";
            // 
            // textProp
            // 
            this.textProp.Location = new System.Drawing.Point(79, 168);
            this.textProp.Name = "textProp";
            this.textProp.Size = new System.Drawing.Size(145, 25);
            this.textProp.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 19);
            this.label4.TabIndex = 19;
            this.label4.Text = "CPF:";
            // 
            // textNasci
            // 
            this.textNasci.Location = new System.Drawing.Point(446, 94);
            this.textNasci.Name = "textNasci";
            this.textNasci.Size = new System.Drawing.Size(100, 25);
            this.textNasci.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(356, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 19);
            this.label3.TabIndex = 17;
            this.label3.Text = "Nascimento:";
            // 
            // textSexo
            // 
            this.textSexo.Location = new System.Drawing.Point(241, 94);
            this.textSexo.Name = "textSexo";
            this.textSexo.Size = new System.Drawing.Size(100, 25);
            this.textSexo.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "Sexo:";
            // 
            // textCor
            // 
            this.textCor.Location = new System.Drawing.Point(79, 97);
            this.textCor.Name = "textCor";
            this.textCor.Size = new System.Drawing.Size(100, 25);
            this.textCor.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 19);
            this.label1.TabIndex = 13;
            this.label1.Text = "Cor:";
            // 
            // textRaca
            // 
            this.textRaca.Location = new System.Drawing.Point(292, 57);
            this.textRaca.Name = "textRaca";
            this.textRaca.Size = new System.Drawing.Size(254, 25);
            this.textRaca.TabIndex = 12;
            // 
            // labelRaca
            // 
            this.labelRaca.AutoSize = true;
            this.labelRaca.Location = new System.Drawing.Point(246, 60);
            this.labelRaca.Name = "labelRaca";
            this.labelRaca.Size = new System.Drawing.Size(40, 19);
            this.labelRaca.TabIndex = 11;
            this.labelRaca.Text = "Raça:";
            // 
            // textNome
            // 
            this.textNome.Location = new System.Drawing.Point(79, 57);
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(145, 25);
            this.textNome.TabIndex = 6;
            // 
            // addPacient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 352);
            this.Controls.Add(this.titlePanel);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "addPacient";
            this.Text = "Pepet - Detalhes do Paciente";
            this.panelControls.ResumeLayout(false);
            this.titlePanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelCode;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.Label labelAnimal;
        private System.Windows.Forms.TextBox textAnimal;
        private System.Windows.Forms.Label labelSangue;
        private System.Windows.Forms.TextBox textSangue;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textRaca;
        private System.Windows.Forms.Label labelRaca;
        private System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textNasci;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textSexo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textCor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textProp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCode;
    }
}