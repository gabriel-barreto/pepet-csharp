﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pepet.Views
{
    public partial class addAnalisys : Form
    {
        public addAnalisys()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addAnalisys));
            this.textCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textPrescricao = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textExames = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textSistemas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textSintomas = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.labelCode = new System.Windows.Forms.Label();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panelControls = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textDate = new System.Windows.Forms.DateTimePicker();
            this.textAnimal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textObs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.titlePanel.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textCode
            // 
            this.textCode.Location = new System.Drawing.Point(90, 17);
            this.textCode.Name = "textCode";
            this.textCode.Size = new System.Drawing.Size(75, 20);
            this.textCode.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 220);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Data:";
            // 
            // textPrescricao
            // 
            this.textPrescricao.Location = new System.Drawing.Point(90, 177);
            this.textPrescricao.Name = "textPrescricao";
            this.textPrescricao.Size = new System.Drawing.Size(456, 20);
            this.textPrescricao.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Prescrição: ";
            // 
            // textExames
            // 
            this.textExames.Location = new System.Drawing.Point(90, 137);
            this.textExames.Name = "textExames";
            this.textExames.Size = new System.Drawing.Size(456, 20);
            this.textExames.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Exames:";
            // 
            // textSistemas
            // 
            this.textSistemas.Location = new System.Drawing.Point(135, 97);
            this.textSistemas.Name = "textSistemas";
            this.textSistemas.Size = new System.Drawing.Size(411, 20);
            this.textSistemas.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Sistemas Acometidos:";
            // 
            // textSintomas
            // 
            this.textSintomas.Location = new System.Drawing.Point(90, 57);
            this.textSintomas.Name = "textSintomas";
            this.textSintomas.Size = new System.Drawing.Size(456, 20);
            this.textSintomas.TabIndex = 6;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(20, 60);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(56, 13);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Sintomas: ";
            // 
            // labelCode
            // 
            this.labelCode.AutoSize = true;
            this.labelCode.Location = new System.Drawing.Point(20, 20);
            this.labelCode.Name = "labelCode";
            this.labelCode.Size = new System.Drawing.Size(43, 13);
            this.labelCode.TabIndex = 3;
            this.labelCode.Text = "Código:";
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.titlePanel.Controls.Add(this.labelTitle);
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(585, 50);
            this.titlePanel.TabIndex = 4;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(12, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(560, 50);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ADIÇÃO DE ANAMNESE";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(371, 20);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(175, 35);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Salvar Alterações";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click_1);
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.buttonSave);
            this.panelControls.Location = new System.Drawing.Point(12, 319);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(560, 75);
            this.panelControls.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textDate);
            this.panel1.Controls.Add(this.textAnimal);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textObs);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textCode);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textPrescricao);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textExames);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textSistemas);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textSintomas);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Controls.Add(this.labelCode);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 257);
            this.panel1.TabIndex = 3;
            // 
            // textDate
            // 
            this.textDate.Location = new System.Drawing.Point(87, 217);
            this.textDate.Name = "textDate";
            this.textDate.Size = new System.Drawing.Size(148, 20);
            this.textDate.TabIndex = 40;
            // 
            // textAnimal
            // 
            this.textAnimal.Location = new System.Drawing.Point(471, 17);
            this.textAnimal.Name = "textAnimal";
            this.textAnimal.Size = new System.Drawing.Size(75, 20);
            this.textAnimal.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(388, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Código Animal:";
            // 
            // textObs
            // 
            this.textObs.Location = new System.Drawing.Point(279, 217);
            this.textObs.Name = "textObs";
            this.textObs.Size = new System.Drawing.Size(267, 20);
            this.textObs.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Obs.:";
            // 
            // addAnalisys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 402);
            this.Controls.Add(this.titlePanel);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "addAnalisys";
            this.Text = "Pepet - Adição Anamnese";
            this.titlePanel.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void titlePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelTitle_Click(object sender, EventArgs e)
        {

        }

        private void panelControls_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelTipo_Click(object sender, EventArgs e)
        {

        }

        private void agendaDataText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void property2phone_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void property2nome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void property1phone_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void property1name_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textRaca_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelRaca_Click(object sender, EventArgs e)
        {

        }

        private void textSangue_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelSangue_Click(object sender, EventArgs e)
        {

        }

        private void textAnimal_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelAnimal_Click(object sender, EventArgs e)
        {

        }

        private void textNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelNome_Click(object sender, EventArgs e)
        {

        }

        private void labelCode_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private TextBox textCode;
        private Label label11;
        private TextBox textPrescricao;
        private Label label8;
        private TextBox textExames;
        private Label label4;
        private TextBox textSistemas;
        private Label label1;
        private TextBox textSintomas;
        private Label labelNome;
        private Label labelCode;
        private Panel titlePanel;
        private Label labelTitle;
        private Button buttonSave;
        private Panel panelControls;
        private Panel panel1;
        private TextBox textObs;
        private Label label2;
        private TextBox textAnimal;
        private Label label3;
        private DateTimePicker textDate;
    }
}
