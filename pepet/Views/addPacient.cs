﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Views
{
    public partial class addPacient : Form
    {
        public addPacient()
        {
            InitializeComponent();
        }
        Services.dbServices dbServices = new Services.dbServices();
        Services.pacientesServices pacientesServices = new Services.pacientesServices();

        private void savePacient(object sender, EventArgs e)
        {
            var paciente = new Models.Pacientes();

            dbServices.connection();

            var collection = dbServices.db.GetCollection<Models.Proprietarios>("proprietarios");
            var query = Query.EQ("cpf", textProp.Text);
            var prop = collection.FindOne(query);

            paciente.code = textBoxCode.Text;
            paciente.nome = textNome.Text;
            paciente.animal = textAnimal.Text;
            paciente.tipoSangue = textSangue.Text;
            paciente.sexo = textSexo.Text;
            paciente.nascimento = textNasci.Text;
            paciente.raca = textRaca.Text;
            paciente.cor = textCor.Text;

            if (prop == null)
            {
                MessageBox.Show("Este proprietário não foi encontrado em nossa base de dados, por favor, informe um proprietário já cadastro ou faça o cadastro de um proprietário antes de cadastrar o paciente!", "Erro - Prorpeitário não encontrado!");
            }
            else {
                MessageBox.Show("Nome: "+prop.nome, "Prorpeitário encontrado!");
                paciente.proprietario = new BsonDocument() { { "nome", prop.nome }, { "telefone", prop.telefone }, { "cpf", prop.cpf } };
                addPaciente(paciente);
            }
        }

        private void addPaciente(Models.Pacientes paciente) {
            pacientesServices.addPaciente(paciente);
            MessageBox.Show("Paciente adicionado com sucesso!", "Sucesso!");
        }
    }
}
