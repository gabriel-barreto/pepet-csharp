﻿namespace pepet.Views
{
    partial class propertierDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(propertierDetails));
            this.labelTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textUf = new System.Windows.Forms.TextBox();
            this.labelTipo = new System.Windows.Forms.Label();
            this.textTel1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textCidade = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBairro = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textNumero = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textLogradouro = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textCep = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textCPF = new System.Windows.Forms.TextBox();
            this.labelRaca = new System.Windows.Forms.Label();
            this.textNome = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(126)))), ((int)(((byte)(34)))));
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(0, -1);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(570, 50);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "DETALHES DO PROPRIETÁRIO";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textEmail);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textUf);
            this.panel1.Controls.Add(this.labelTipo);
            this.panel1.Controls.Add(this.textTel1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textCidade);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBairro);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textNumero);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textLogradouro);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textCep);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textCPF);
            this.panel1.Controls.Add(this.labelRaca);
            this.panel1.Controls.Add(this.textNome);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Location = new System.Drawing.Point(5, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(565, 224);
            this.panel1.TabIndex = 4;
            // 
            // textEmail
            // 
            this.textEmail.BackColor = System.Drawing.Color.White;
            this.textEmail.Location = new System.Drawing.Point(296, 181);
            this.textEmail.Name = "textEmail";
            this.textEmail.ReadOnly = true;
            this.textEmail.Size = new System.Drawing.Size(250, 20);
            this.textEmail.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Email:";
            // 
            // textUf
            // 
            this.textUf.BackColor = System.Drawing.Color.White;
            this.textUf.Location = new System.Drawing.Point(495, 141);
            this.textUf.Name = "textUf";
            this.textUf.ReadOnly = true;
            this.textUf.Size = new System.Drawing.Size(51, 20);
            this.textUf.TabIndex = 32;
            // 
            // labelTipo
            // 
            this.labelTipo.AutoSize = true;
            this.labelTipo.Location = new System.Drawing.Point(465, 144);
            this.labelTipo.Name = "labelTipo";
            this.labelTipo.Size = new System.Drawing.Size(24, 13);
            this.labelTipo.TabIndex = 31;
            this.labelTipo.Text = "UF:";
            // 
            // textTel1
            // 
            this.textTel1.BackColor = System.Drawing.Color.White;
            this.textTel1.Location = new System.Drawing.Point(90, 181);
            this.textTel1.Name = "textTel1";
            this.textTel1.ReadOnly = true;
            this.textTel1.Size = new System.Drawing.Size(145, 20);
            this.textTel1.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Telefone 1:";
            // 
            // textCidade
            // 
            this.textCidade.BackColor = System.Drawing.Color.White;
            this.textCidade.Location = new System.Drawing.Point(299, 141);
            this.textCidade.Name = "textCidade";
            this.textCidade.ReadOnly = true;
            this.textCidade.Size = new System.Drawing.Size(145, 20);
            this.textCidade.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Cidade:";
            // 
            // textBairro
            // 
            this.textBairro.BackColor = System.Drawing.Color.White;
            this.textBairro.Location = new System.Drawing.Point(90, 141);
            this.textBairro.Name = "textBairro";
            this.textBairro.ReadOnly = true;
            this.textBairro.Size = new System.Drawing.Size(145, 20);
            this.textBairro.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Bairro:";
            // 
            // textNumero
            // 
            this.textNumero.BackColor = System.Drawing.Color.White;
            this.textNumero.Location = new System.Drawing.Point(432, 101);
            this.textNumero.Name = "textNumero";
            this.textNumero.ReadOnly = true;
            this.textNumero.Size = new System.Drawing.Size(114, 20);
            this.textNumero.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(379, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Número:";
            // 
            // textLogradouro
            // 
            this.textLogradouro.BackColor = System.Drawing.Color.White;
            this.textLogradouro.Location = new System.Drawing.Point(90, 101);
            this.textLogradouro.Name = "textLogradouro";
            this.textLogradouro.ReadOnly = true;
            this.textLogradouro.Size = new System.Drawing.Size(263, 20);
            this.textLogradouro.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Logradouro:";
            // 
            // textCep
            // 
            this.textCep.BackColor = System.Drawing.Color.White;
            this.textCep.Location = new System.Drawing.Point(90, 61);
            this.textCep.Name = "textCep";
            this.textCep.ReadOnly = true;
            this.textCep.Size = new System.Drawing.Size(100, 20);
            this.textCep.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "CEP:";
            // 
            // textCPF
            // 
            this.textCPF.BackColor = System.Drawing.Color.White;
            this.textCPF.Location = new System.Drawing.Point(359, 21);
            this.textCPF.Name = "textCPF";
            this.textCPF.ReadOnly = true;
            this.textCPF.Size = new System.Drawing.Size(187, 20);
            this.textCPF.TabIndex = 12;
            // 
            // labelRaca
            // 
            this.labelRaca.AutoSize = true;
            this.labelRaca.Location = new System.Drawing.Point(323, 24);
            this.labelRaca.Name = "labelRaca";
            this.labelRaca.Size = new System.Drawing.Size(30, 13);
            this.labelRaca.TabIndex = 11;
            this.labelRaca.Text = "CPF:";
            // 
            // textNome
            // 
            this.textNome.BackColor = System.Drawing.Color.White;
            this.textNome.Location = new System.Drawing.Point(90, 21);
            this.textNome.Name = "textNome";
            this.textNome.ReadOnly = true;
            this.textNome.Size = new System.Drawing.Size(176, 20);
            this.textNome.TabIndex = 6;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(20, 24);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(41, 13);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Nome: ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(301, 293);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 37);
            this.button1.TabIndex = 6;
            this.button1.Text = "Excluir Proprietário";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // propertierDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 342);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "propertierDetails";
            this.Text = "Pepet";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTipo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textCPF;
        private System.Windows.Forms.Label labelRaca;
        public System.Windows.Forms.TextBox textNome;
        private System.Windows.Forms.Label labelNome;
        public System.Windows.Forms.TextBox textCidade;
        public System.Windows.Forms.TextBox textBairro;
        public System.Windows.Forms.TextBox textNumero;
        public System.Windows.Forms.TextBox textLogradouro;
        public System.Windows.Forms.TextBox textCep;
        public System.Windows.Forms.TextBox textEmail;
        public System.Windows.Forms.TextBox textUf;
        public System.Windows.Forms.TextBox textTel1;
        private System.Windows.Forms.Button button1;
    }
}