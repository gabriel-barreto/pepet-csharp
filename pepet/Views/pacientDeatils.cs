﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Views
{
    public partial class pacientDeatils : Form
    {
        public pacientDeatils()
        {
            InitializeComponent();
        }

        Services.dbServices dbServices = new Services.dbServices();
        Services.pacientesServices pacientesServices = new Services.pacientesServices();
        Services.atendimentosServices atendimentoServices = new Services.atendimentosServices();
        Services.proprietariosServices proprietariosServices = new Services.proprietariosServices();

        Filters.cpfFilter cpfFilter = new Filters.cpfFilter();

        Views.propertierDetails propDetails = new propertierDetails();
        Views.AnalisysDetails analisysDetails = new AnalisysDetails();

        private void goToProprietario(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel link = (LinkLabel)sender;
            var prop = proprietariosServices.getProprietarioByCpf(cpfFilter.unmask(link.Text));
            if (prop != null)
            {
                propDetails.textCPF.Text = prop.cpf;
                propDetails.textNome.Text = prop.nome;
                propDetails.textEmail.Text = prop.email;
                propDetails.textTel1.Text = prop.telefone;
                propDetails.textLogradouro.Text = prop.endereco["log"].AsString;
                propDetails.textNumero.Text = prop.endereco["num"].AsString;
                propDetails.textBairro.Text = prop.endereco["bai"].AsString;
                propDetails.textCidade.Text = prop.endereco["cid"].AsString;
                propDetails.textCep.Text = prop.endereco["cep"].AsString;
                propDetails.textUf.Text = prop.endereco["est"].AsString;

                propDetails.ShowDialog();
            }
            else {
                MessageBox.Show("Proprietário náo encontrado!", "Erro 404 - Not Found");
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja realmente deletar o paciente?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                pacientesServices.deletePaciente(textCode.Text);
                MessageBox.Show("Paciente deletado com sucesso!", "Sucesso!");
                this.Close();
            }
        }

        private void linkLabelCodeAtendimento_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel link = linkLabelCodeAtendimento;
            var atendimento = atendimentoServices.getAtendimentoByCode(link.Text);

            if (atendimento != null)
            {
                analisysDetails.textCode.Text = atendimento.code;
                analisysDetails.textAnimal.Text = atendimento.animal;
                analisysDetails.textData.Text = atendimento.data;
                analisysDetails.textExames.Text = atendimento.exames;
                analisysDetails.textObs.Text = atendimento.obs;
                analisysDetails.textPrescri.Text = atendimento.prescricao;
                analisysDetails.textSintomas.Text = atendimento.sintomas;
                analisysDetails.textSistemas.Text = atendimento.sintomasAcometidos;

                analisysDetails.ShowDialog();

            }
            else {
                MessageBox.Show("Atendimento não encontrado", "Error 404 - Not Found");
            }
        }
    }
}
