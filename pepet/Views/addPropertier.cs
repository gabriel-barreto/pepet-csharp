﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pepet.Views
{
    public partial class addPropertier : Form
    {
        public addPropertier()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addPropertier));
            this.textUf = new System.Windows.Forms.TextBox();
            this.labelTipo = new System.Windows.Forms.Label();
            this.textTel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textCid = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBai = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textNum = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textLog = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textCep = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textCpf = new System.Windows.Forms.TextBox();
            this.labelRaca = new System.Windows.Forms.Label();
            this.textNome = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panelControls = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.linkLabelCEP = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.titlePanel.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textUf
            // 
            this.textUf.Enabled = false;
            this.textUf.Location = new System.Drawing.Point(491, 136);
            this.textUf.Name = "textUf";
            this.textUf.Size = new System.Drawing.Size(51, 20);
            this.textUf.TabIndex = 32;
            // 
            // labelTipo
            // 
            this.labelTipo.AutoSize = true;
            this.labelTipo.Location = new System.Drawing.Point(461, 139);
            this.labelTipo.Name = "labelTipo";
            this.labelTipo.Size = new System.Drawing.Size(24, 13);
            this.labelTipo.TabIndex = 31;
            this.labelTipo.Text = "UF:";
            // 
            // textTel
            // 
            this.textTel.Location = new System.Drawing.Point(86, 176);
            this.textTel.Name = "textTel";
            this.textTel.Size = new System.Drawing.Size(128, 20);
            this.textTel.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 179);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Telefone:";
            // 
            // textCid
            // 
            this.textCid.Enabled = false;
            this.textCid.Location = new System.Drawing.Point(295, 136);
            this.textCid.Name = "textCid";
            this.textCid.Size = new System.Drawing.Size(145, 20);
            this.textCid.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Cidade:";
            // 
            // textBai
            // 
            this.textBai.Enabled = false;
            this.textBai.Location = new System.Drawing.Point(86, 136);
            this.textBai.Name = "textBai";
            this.textBai.Size = new System.Drawing.Size(145, 20);
            this.textBai.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Bairro:";
            // 
            // textNum
            // 
            this.textNum.Location = new System.Drawing.Point(428, 96);
            this.textNum.Name = "textNum";
            this.textNum.Size = new System.Drawing.Size(114, 20);
            this.textNum.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(375, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Número:";
            // 
            // textLog
            // 
            this.textLog.Enabled = false;
            this.textLog.Location = new System.Drawing.Point(86, 96);
            this.textLog.Name = "textLog";
            this.textLog.Size = new System.Drawing.Size(263, 20);
            this.textLog.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Logradouro:";
            // 
            // textCep
            // 
            this.textCep.Location = new System.Drawing.Point(86, 56);
            this.textCep.Name = "textCep";
            this.textCep.Size = new System.Drawing.Size(100, 20);
            this.textCep.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "CEP:";
            // 
            // textCpf
            // 
            this.textCpf.Location = new System.Drawing.Point(355, 16);
            this.textCpf.Name = "textCpf";
            this.textCpf.Size = new System.Drawing.Size(187, 20);
            this.textCpf.TabIndex = 12;
            // 
            // labelRaca
            // 
            this.labelRaca.AutoSize = true;
            this.labelRaca.Location = new System.Drawing.Point(319, 19);
            this.labelRaca.Name = "labelRaca";
            this.labelRaca.Size = new System.Drawing.Size(30, 13);
            this.labelRaca.TabIndex = 11;
            this.labelRaca.Text = "CPF:";
            // 
            // textNome
            // 
            this.textNome.Location = new System.Drawing.Point(86, 16);
            this.textNome.Name = "textNome";
            this.textNome.Size = new System.Drawing.Size(176, 20);
            this.textNome.TabIndex = 6;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(16, 19);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(41, 13);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Nome: ";
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(126)))), ((int)(((byte)(34)))));
            this.titlePanel.Controls.Add(this.labelTitle);
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(585, 50);
            this.titlePanel.TabIndex = 4;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(12, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(560, 50);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ADIÇÃO DE PROPRIETÁRIO";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(371, 20);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(175, 35);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Salvar Alterações";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click_1);
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.buttonSave);
            this.panelControls.Location = new System.Drawing.Point(12, 277);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(560, 75);
            this.panelControls.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.textEmail);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.linkLabelCEP);
            this.panel1.Controls.Add(this.textUf);
            this.panel1.Controls.Add(this.labelTipo);
            this.panel1.Controls.Add(this.textTel);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textCid);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBai);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textNum);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textLog);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textCep);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textCpf);
            this.panel1.Controls.Add(this.labelRaca);
            this.panel1.Controls.Add(this.textNome);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 215);
            this.panel1.TabIndex = 3;
            // 
            // linkLabelCEP
            // 
            this.linkLabelCEP.AutoSize = true;
            this.linkLabelCEP.Location = new System.Drawing.Point(192, 59);
            this.linkLabelCEP.Name = "linkLabelCEP";
            this.linkLabelCEP.Size = new System.Drawing.Size(95, 13);
            this.linkLabelCEP.TabIndex = 34;
            this.linkLabelCEP.TabStop = true;
            this.linkLabelCEP.Text = "Procurar endereço";
            this.linkLabelCEP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelCEP_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Email:";
            // 
            // textEmail
            // 
            this.textEmail.Location = new System.Drawing.Point(279, 176);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(263, 20);
            this.textEmail.TabIndex = 36;
            // 
            // addPropertier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.titlePanel);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "addPropertier";
            this.Text = "Pepet - Adição de Proprietário";
            this.titlePanel.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private TextBox textUf;
        private Label labelTipo;
        private TextBox textTel;
        private Label label11;
        private TextBox textCid;
        private Label label7;
        private TextBox textBai;
        private Label label8;
        private TextBox textNum;
        private Label label6;
        private TextBox textLog;
        private Label label4;
        private TextBox textCep;
        private Label label1;
        private TextBox textCpf;
        private Label labelRaca;
        private TextBox textNome;
        private Label labelNome;
        private Panel titlePanel;
        private Label labelTitle;
        private Button buttonSave;
        private Panel panelControls;
        private Panel panel1;
        private LinkLabel linkLabelCEP;

        Services.proprietariosServices propServices = new Services.proprietariosServices();

        private void buttonSave_Click_1(object sender, EventArgs e)
        {
            var newProprietario = new Models.Proprietarios();

            newProprietario.cpf = textCpf.Text;
            newProprietario.nome = textNome.Text;
            newProprietario.telefone = textTel.Text;
            newProprietario.email = textEmail.Text;
            newProprietario.endereco = new MongoDB.Bson.BsonDocument() {
                { "cep", textCep.Text },
                { "log", textLog.Text },
                { "num", textNum.Text },
                { "bai", textBai.Text },
                { "cid", textCid.Text },
                { "est", textUf.Text  }
            };

            propServices.addProprietario(newProprietario);
        }

        Services.cepServices cepServices = new Services.cepServices();

        private void linkLabelCEP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (textCep.Text.Length == 8) {
                var end = cepServices.getAddress(textCep.Text);
                textLog.Text = end[0];
                textBai.Text = end[1];
                textCid.Text = end[2];
                textUf.Text = end[3];
                textNum.Focus();
            }
        }

        private TextBox textEmail;
        private Label label2;
    }
}
