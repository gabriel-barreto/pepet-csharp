﻿namespace pepet.Views
{
    partial class AnalisysDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnalisysDetails));
            this.labelTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textAnimal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textObs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textPrescri = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textExames = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textSistemas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textSintomas = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.labelCode = new System.Windows.Forms.Label();
            this.textData = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(1, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(560, 50);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "DETALHES DO ATENDIMENTO";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textData);
            this.panel1.Controls.Add(this.textAnimal);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textObs);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textCode);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textPrescri);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textExames);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textSistemas);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textSintomas);
            this.panel1.Controls.Add(this.labelNome);
            this.panel1.Controls.Add(this.labelCode);
            this.panel1.Location = new System.Drawing.Point(1, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 257);
            this.panel1.TabIndex = 4;
            // 
            // textAnimal
            // 
            this.textAnimal.BackColor = System.Drawing.Color.White;
            this.textAnimal.Enabled = false;
            this.textAnimal.Location = new System.Drawing.Point(471, 17);
            this.textAnimal.Name = "textAnimal";
            this.textAnimal.ReadOnly = true;
            this.textAnimal.Size = new System.Drawing.Size(75, 20);
            this.textAnimal.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(388, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Código Animal:";
            // 
            // textObs
            // 
            this.textObs.BackColor = System.Drawing.Color.White;
            this.textObs.Enabled = false;
            this.textObs.Location = new System.Drawing.Point(279, 217);
            this.textObs.Name = "textObs";
            this.textObs.ReadOnly = true;
            this.textObs.Size = new System.Drawing.Size(267, 20);
            this.textObs.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Obs.:";
            // 
            // textCode
            // 
            this.textCode.BackColor = System.Drawing.Color.White;
            this.textCode.Enabled = false;
            this.textCode.Location = new System.Drawing.Point(90, 17);
            this.textCode.Name = "textCode";
            this.textCode.ReadOnly = true;
            this.textCode.Size = new System.Drawing.Size(75, 20);
            this.textCode.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 220);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Data:";
            // 
            // textPrescri
            // 
            this.textPrescri.BackColor = System.Drawing.Color.White;
            this.textPrescri.Enabled = false;
            this.textPrescri.Location = new System.Drawing.Point(90, 177);
            this.textPrescri.Name = "textPrescri";
            this.textPrescri.ReadOnly = true;
            this.textPrescri.Size = new System.Drawing.Size(456, 20);
            this.textPrescri.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Prescrição: ";
            // 
            // textExames
            // 
            this.textExames.BackColor = System.Drawing.Color.White;
            this.textExames.Enabled = false;
            this.textExames.Location = new System.Drawing.Point(90, 137);
            this.textExames.Name = "textExames";
            this.textExames.ReadOnly = true;
            this.textExames.Size = new System.Drawing.Size(456, 20);
            this.textExames.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Exames:";
            // 
            // textSistemas
            // 
            this.textSistemas.BackColor = System.Drawing.Color.White;
            this.textSistemas.Enabled = false;
            this.textSistemas.Location = new System.Drawing.Point(135, 97);
            this.textSistemas.Name = "textSistemas";
            this.textSistemas.ReadOnly = true;
            this.textSistemas.Size = new System.Drawing.Size(411, 20);
            this.textSistemas.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Sistemas Acometidos:";
            // 
            // textSintomas
            // 
            this.textSintomas.BackColor = System.Drawing.Color.White;
            this.textSintomas.Enabled = false;
            this.textSintomas.Location = new System.Drawing.Point(90, 57);
            this.textSintomas.Name = "textSintomas";
            this.textSintomas.ReadOnly = true;
            this.textSintomas.Size = new System.Drawing.Size(456, 20);
            this.textSintomas.TabIndex = 6;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(20, 60);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(56, 13);
            this.labelNome.TabIndex = 5;
            this.labelNome.Text = "Sintomas: ";
            // 
            // labelCode
            // 
            this.labelCode.AutoSize = true;
            this.labelCode.Location = new System.Drawing.Point(20, 20);
            this.labelCode.Name = "labelCode";
            this.labelCode.Size = new System.Drawing.Size(43, 13);
            this.labelCode.TabIndex = 3;
            this.labelCode.Text = "Código:";
            // 
            // textData
            // 
            this.textData.BackColor = System.Drawing.Color.White;
            this.textData.Enabled = false;
            this.textData.Location = new System.Drawing.Point(90, 217);
            this.textData.Name = "textData";
            this.textData.ReadOnly = true;
            this.textData.Size = new System.Drawing.Size(145, 20);
            this.textData.TabIndex = 39;
            // 
            // AnalisysDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(561, 321);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "AnalisysDetails";
            this.Text = "Pepet";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox textAnimal;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textObs;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textCode;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox textPrescri;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox textExames;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textSistemas;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textSintomas;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.Label labelCode;
        public System.Windows.Forms.TextBox textData;
    }
}