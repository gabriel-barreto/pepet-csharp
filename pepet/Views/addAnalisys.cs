﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pepet.Views
{
    public partial class addAnalisys : Form
    {

        Services.atendimentosServices atendimentosServices = new Services.atendimentosServices();

        private void buttonSave_Click_1(object sender, EventArgs e)
        {
            var newAtendimento = new Models.Analises();

            newAtendimento.animal = textAnimal.Text;
            newAtendimento.code = textCode.Text;
            newAtendimento.exames = textExames.Text;
            newAtendimento.prescricao = textPrescricao.Text;
            newAtendimento.sintomas = textSintomas.Text;
            newAtendimento.sintomas = textSistemas.Text;
            newAtendimento.data = textDate.Text;
            newAtendimento.obs = textObs.Text;

            atendimentosServices.addAtendimento(newAtendimento);
        }
    }
}
