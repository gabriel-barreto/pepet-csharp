﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        Services.dbServices dbServices = new Services.dbServices();
        Services.cepServices cepServices = new Services.cepServices();
        Services.pacientesServices pacientesServices = new Services.pacientesServices();
        Services.atendimentosServices atendimentosServices = new Services.atendimentosServices();

        Views.pacientDeatils pacientDetails = new Views.pacientDeatils();

        Views.addPacient addPacient = new Views.addPacient();
        Views.addPropertier addPropertier = new Views.addPropertier();
        Views.addAnalisys addAnalisys = new Views.addAnalisys();

        Filters.cpfFilter cpfFilter = new Filters.cpfFilter();

        private void mainInit(object sender, EventArgs e)
        {
            dbServices.connection();
        }

        public void buttonSearchOnClick(object sender, EventArgs e)
        {
            var criterio = (searchPacientField.Text == "Nome" ? "nome" : "code");
            var filtro = searchPacientData.Text;

            var paciente = new Models.Pacientes();
            if(criterio == "nome") {
                paciente = pacientesServices.getPacienteByName(filtro);
            }

            else if(criterio == "code") {
                paciente = pacientesServices.getPacienteByCode(filtro);
            }

            if (paciente != null)
            {
                pacientDetails.textCode.Text = paciente.code;
                pacientDetails.textNome.Text = paciente.nome;
                pacientDetails.textSangue.Text = paciente.tipoSangue;
                pacientDetails.textProperty1Phone.Text = paciente.proprietario["telefone"].AsString;
                pacientDetails.textProperty1Name.Text = paciente.proprietario["nome"].AsString;
                pacientDetails.cpfLink.Text = cpfFilter.mask((String)paciente.proprietario["cpf"].AsString);
                pacientDetails.textNascimento.Text = paciente.nascimento;
                pacientDetails.textSexo.Text = paciente.sexo;
                pacientDetails.textCor.Text = paciente.cor;
                pacientDetails.textRaca.Text = paciente.raca;
                pacientDetails.textAnimal.Text = paciente.animal;
                var lastAtendimento = atendimentosServices.getLastAtendimento(paciente.code);

                if (lastAtendimento != null)
                {
                    pacientDetails.labelData.Text = lastAtendimento.data;
                    pacientDetails.labelExames.Text = lastAtendimento.exames;
                    pacientDetails.labelObs.Text = lastAtendimento.obs;
                    pacientDetails.labelPrescricao.Text = lastAtendimento.prescricao;
                    pacientDetails.labelSintomas.Text = lastAtendimento.sintomas;
                    pacientDetails.labelSistemas.Text = lastAtendimento.sintomasAcometidos;
                    pacientDetails.linkLabelCodeAtendimento.Text = lastAtendimento.code;
                }
                else {
                    pacientDetails.labelData.Text = "Não há atendimentos registrados para esse animal";
                }

                pacientDetails.ShowDialog();
            }
        }

        private Models.Pacientes searchPaciente(string criterio, string filtro) {
            var collection = dbServices.db.GetCollection<Models.Pacientes>("pacientes");
            var query = Query.EQ(criterio, filtro);
            var paciente = collection.FindOne(query);
            if (paciente == null)
            {
                MessageBox.Show("Nenhum documento foi encontrado!", "404 - Not Found");
            }

            return paciente;
        }

        private void searchClean() {
            searchPacientData.Clear();
            searchPacientData.Focus();
        }

        private void openAddPropertier(object sender, EventArgs e) {
            addPropertier.ShowDialog();
        }

        private void openAddPacient(object sender, EventArgs e) {
            addPacient.ShowDialog();
        }

        private void openAddAnalisys(object sender, EventArgs e) {
            addAnalisys.ShowDialog();
        }
    }
}
