﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Models
{
    class Proprietarios
    {
        public ObjectId _id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public BsonDocument endereco { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public string __v { get; set; }
   }
}
