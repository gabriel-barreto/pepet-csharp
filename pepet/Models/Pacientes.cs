﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Models
{
    class Pacientes
    {
        public ObjectId _id { get; set; }
        public string code { get; set; }
        public string animal { get; set; }
        public string nome { get; set; }
        public string raca { get; set; }
        public string cor { get; set; }
        public string sexo { get; set; }
        public string nascimento { get; set; }
        public string tipoSangue { get; set; }
        public BsonDocument proprietario { get; set; }
        public string __v { get; set; }
    }
}
