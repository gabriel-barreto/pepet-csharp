﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace pepet.Models
{
    class Analises
    {
        public ObjectId _id { get; set; }
        public string code { get; set; }
        public string animal { get; set; }
        public string sintomas { get; set; }
        public string sintomasAcometidos { get; set; }
        public string exames { get; set; }
        public string prescricao { get; set; }
        public string data { get; set; }
        public string obs { get; set; }
        public string __v { get; set; }
    }
}
