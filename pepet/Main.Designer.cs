﻿namespace pepet
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelSearch = new System.Windows.Forms.Label();
            this.searchPacientField = new System.Windows.Forms.ComboBox();
            this.searchPacientData = new System.Windows.Forms.TextBox();
            this.LabelsearchPacientFilter = new System.Windows.Forms.Label();
            this.LabelsearchPacientData = new System.Windows.Forms.Label();
            this.buttonSearchPacient = new System.Windows.Forms.Button();
            this.searchPacientWrap = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.searchPacientWrap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(113)))));
            resources.ApplyResources(this.button1, "button1");
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.openAddAnalisys);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(126)))), ((int)(((byte)(34)))));
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(126)))), ((int)(((byte)(34)))));
            resources.ApplyResources(this.button3, "button3");
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.openAddPropertier);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(188)))), ((int)(((byte)(156)))));
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(188)))), ((int)(((byte)(156)))));
            resources.ApplyResources(this.button2, "button2");
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.openAddPacient);
            // 
            // labelSearch
            // 
            resources.ApplyResources(this.labelSearch, "labelSearch");
            this.labelSearch.Name = "labelSearch";
            // 
            // searchPacientField
            // 
            this.searchPacientField.FormattingEnabled = true;
            this.searchPacientField.Items.AddRange(new object[] {
            resources.GetString("searchPacientField.Items"),
            resources.GetString("searchPacientField.Items1")});
            resources.ApplyResources(this.searchPacientField, "searchPacientField");
            this.searchPacientField.Name = "searchPacientField";
            // 
            // searchPacientData
            // 
            resources.ApplyResources(this.searchPacientData, "searchPacientData");
            this.searchPacientData.Name = "searchPacientData";
            // 
            // LabelsearchPacientFilter
            // 
            resources.ApplyResources(this.LabelsearchPacientFilter, "LabelsearchPacientFilter");
            this.LabelsearchPacientFilter.Name = "LabelsearchPacientFilter";
            // 
            // LabelsearchPacientData
            // 
            resources.ApplyResources(this.LabelsearchPacientData, "LabelsearchPacientData");
            this.LabelsearchPacientData.Name = "LabelsearchPacientData";
            // 
            // buttonSearchPacient
            // 
            this.buttonSearchPacient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.buttonSearchPacient.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            resources.ApplyResources(this.buttonSearchPacient, "buttonSearchPacient");
            this.buttonSearchPacient.ForeColor = System.Drawing.Color.White;
            this.buttonSearchPacient.Name = "buttonSearchPacient";
            this.buttonSearchPacient.UseVisualStyleBackColor = false;
            this.buttonSearchPacient.Click += new System.EventHandler(this.buttonSearchOnClick);
            // 
            // searchPacientWrap
            // 
            this.searchPacientWrap.BackColor = System.Drawing.Color.White;
            this.searchPacientWrap.Controls.Add(this.buttonSearchPacient);
            this.searchPacientWrap.Controls.Add(this.LabelsearchPacientData);
            this.searchPacientWrap.Controls.Add(this.LabelsearchPacientFilter);
            this.searchPacientWrap.Controls.Add(this.searchPacientData);
            this.searchPacientWrap.Controls.Add(this.searchPacientField);
            this.searchPacientWrap.Controls.Add(this.labelSearch);
            resources.ApplyResources(this.searchPacientWrap, "searchPacientWrap");
            this.searchPacientWrap.Name = "searchPacientWrap";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pepet.Properties.Resources.pepet;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.searchPacientWrap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Load += new System.EventHandler(this.mainInit);
            this.searchPacientWrap.ResumeLayout(false);
            this.searchPacientWrap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.ComboBox searchPacientField;
        private System.Windows.Forms.TextBox searchPacientData;
        private System.Windows.Forms.Label LabelsearchPacientFilter;
        private System.Windows.Forms.Label LabelsearchPacientData;
        private System.Windows.Forms.Button buttonSearchPacient;
        private System.Windows.Forms.Panel searchPacientWrap;
    }
}

