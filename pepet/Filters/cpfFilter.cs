﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pepet.Filters
{
    class cpfFilter
    {
        public string unmask(string cpf) {
            string output = "";

            string[] cpfList = cpf.Split('.');
            string[] lastMemberList = cpfList[2].Split('-');
            string lastMember = String.Join("", lastMemberList);

            output = cpfList[0] + cpfList[1] + lastMember;

            return output;
        }
        public string mask(String cpf) {
            String output = "";
            int len = cpf.Length;
            output = cpf.Substring(0, 3) + ".";
            output += cpf.Substring(3, 3) + ".";
            output += cpf.Substring(6, 3) + "-";
            output += cpf.Substring(8, 3).Substring(1, 2);
            return output;
        }
    }
}
